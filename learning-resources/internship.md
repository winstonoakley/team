# Internship

What are the practical skills that interns would like to take away from an unpaid educational internship?

* Programming basics:
    * Importing modules
    * conditions
    * loops
* Basic ML understanding (also AI, from big picture perspective)
* Basic web development (HTTP requests, REST APIs, JSON)
* Coding best practices, patterns, anti-patterns, style, linters, and auto-formatters
* Basic of chatbot building: intents, entities, utterances

* Company and vision:
    * Introduction to Tangible AI
    * "How nonprofits use AI" webinar
    * Open source principles
    * Open source licenses and how to choose the right one

## Checklist

This checklist is interactive within gitlab or github, I think.
So students may be able to keep track of their progress within a fork of this repository.
And a bot may be able to follow their progress by tracking their 

### Week 1

- [ ] create gitlab account (do not click "Start Trial")
- [ ] confirm your gitlab email address
- [ ] navigate to gitlab.com/tangibleai/education and click green "Request Access" button
- [ ] send slack message on #interns channel letting us know your gitlab username
    **MANAGER**: add the intern's gitlab username to the Members on the project
- [ ] install Anaconda
- [ ] install bash (git-bash or WSL on Windows)
- [ ] learn these bash file maniuplation commands and apps thoroughly: cd, ls, cat, find, mv, mkdir
- [ ] choose a terminal text editor and learn the hotkeys for creating, editing, saving a file, and exiting (or vim)
- [ ] take this bash quiz once you think you've mastered bash
## Resources

- ["Querying JSON/YML/XML files from the command line"](http://hackerpublicradio.org/eps.php?id=3252) (15 min) by [CRVS](http://hackerpublicradio.org/correspondents.php?hostid=385) on Hacker Public Radio Episode 3252
    - short introduction to json file format
    - 3 cool bash tools for parsing yaml, json, and even xml files.
    - You can "query" the yml/json/xml files like a database so the apps called `yq`, `jq`, and `xq`
    - They also let you convert between each format

- ["How Python Makes Thinking in Code Easier"](https://blog.dropbox.com/topics/work-culture/-the-mind-at-work--guido-van-rossum-on-how-python-makes-thinking)
