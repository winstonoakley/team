# Blog Posts and Hacker Public Radio Script Ideas

## Introduction

I'm Hobson Lane, recording my first podcast for Hacker Public Radio. I've been passionate about AI ever since I was about 10 years old. I used to try to build text adventures with characters (chatbots) that would talk back to me. Yea, I was kind-of a loner.

Decades later, I ran into Maria Dyshel at a Python Study Group and she inspired me to get back into chatbots. We founded Tangible AI to build conversational chatbots and cognitive assistants that actually help people. Our chatbots give people emotional support, access to honest information, and help to bridge the socio-economic divide by supporting nonprofits and social impact businesses.

## Dark Patterns

Today I want to talk about a little dark pattern that prevents people from accessing the information they need to do good in the world.
I teach data science and NLP.
So I'm always on the lookout for interesting public datasets that can help students learn about the power of NLP for doing good.
Unfortunately, in the US, many of our public datasets are difficult to access.

When the Internet was new and only teenagers and geeks knew how to use it, public officials could be forgiven for "publishing" data in PDFs distributed in press releases.
But we live in an era where elected officials responsible for securing voter registration data have the skill to deploy dark pattern websites that support their political agenda.
And the skill to do this sort of sophisticated technical work is not limited to advanced, stable democracies like the United States.
Officials in charge of data in most developing countries are also deploying sophisticated web applications that breach the public trust.
Do a [Duck search]() for ["Brian Kemp suppression"](https://duckduckgo.com/?q=Brian+Kemp+suppression) if you want to learn more.
He was so adept at managing his IT department, he successfully suppressed voters in the United States using dark patterns that made voter registration data accessible only to his supporters and campaign managers.

## Illuminating the Dark

So today I'll show you how easy it is to process data from prosocial public data sources like Wikipedia.
And then I'll show you the problem with some dark patterns on the web.
Some are intentional and some are not, but we'll help you illuminate the data you want and scrape it.


There are no options for the `pd.read_html` function that do what you want. So when I tried to get a list of business names from the California Department of State website, I get everything except the name when Pandas automatically parses the HTML:

```python
>>> bizname = 'poss'
>>> url = f'https://businesssearch.sos.ca.gov/CBS/SearchResults?filing=&SearchType=CORP&SearchCriteria={bizname}&SearchSubType=Begins'
>>> df = pd.read_html(url)[0]
>>> df
  Entity Number Registration Date         Status                                        Entity Name Jurisdiction      Agent for Service of Process
0      C2645412        04/02/2004         ACTIVE  View details for entity number 02645412  POSSU...      GEORGIA   ERESIDENTAGENT, INC. (C2702827)
1      C0786330        09/22/1976      DISSOLVED  View details for entity number 00786330  POSSU...   CALIFORNIA                        I. HALPERN
2      C2334141        03/01/2001  FTB SUSPENDED  View details for entity number 02334141  POSSU...   CALIFORNIA                   CLAIR G BURRILL
3      C0658630        11/08/1972  FTB SUSPENDED  View details for entity number 00658630  POSSU...   CALIFORNIA                               NaN
4      C1713121        09/23/1992  FTB SUSPENDED  View details for entity number 01713121  POSSU...   CALIFORNIA                LAWRENCE J. TURNER
5      C1207820        08/05/1983      DISSOLVED  View details for entity number 01207820  POSSU...   CALIFORNIA                          R L CARL
6      C3921531        06/27/2016         ACTIVE  View details for entity number 03921531  POSSU...   CALIFORNIA  REGISTERED AGENTS INC (C3365816)
```

The website hides business names behind a button.
But you can use `requests` to download the raw html.
Then you can use `bs4` to extract the raw HTML table as well as any particular row (`<tr>`) or cell (`<td>`) that you want.

First lets see how public APIs and the semantic web are supposed to work.
Say I read a great SciFi novel, Three Body Problem and wanted to find other books that, like it, won the Hugo Award for best novel.
This is how you search for something on wikipedia:

```python
import requests
base_url = 'https://en.wikipedia.org'
search_text = 'hugo award best novel liu'
search_results = requests.get(
    'https://en.wikipedia.org/w/index.php',
    {'search': search_text},
    )
search_results
# <Response [200]>
```

Now we can programmatically find the page with the Hugo Awards using Beutiful Soup 4.
Don't try to install Beautiful Soup without tacking on that version 4 to the end.
Otherwise you'll get some confusing error messages.
And the import name is `bs4`, not beautifulsoup.
The `.find()` method finds the first element in an BeautifulSoup object.
So if you want to walk through more pages than just this first search result, us `.findall()`.

You only need the first search result for this carefully crafted search ;):

```python
import bs4
soup = bs4.BeautifulSoup(search_results.text)
soup.find('div', {'class': 'searchresults'})
soup = (soup.find('div', {'class': 'searchresults'}) or soup).find('ul')
hugo_url = (soup.find('li') or soup).find('a', href=True).get('href')
hugo_url
# '/wiki/Hugo_Award_for_Best_Novel'
```

So now we can join the wikipedia path with the base_url to get to the page containing the data table we're looking for.
And we can use Pandas to deal download and parse it directly, without any fancy BeaufulSouping.

```python


Some of this code is on stack overflow in the answer to ["Pandas read_html to return raw HTML"](https://stackoverflow.com/a/65755142/623735)

```python
>>> soup = bs4.BeautifulSoup(requests.get(url).text)
>>> table = soup.find('table').findAll('tr')
>>> names = []
... for row in table:
...     names.append(getattr(row.find('button'), 'contents', [''])[0].strip())
>>> names
['',
 'POSSUM FILMS, INC',
 'POSSUM INC.',
 'POSSUM MEDIA, INC.',
 'POSSUM POINT PRODUCTIONS, INC.',
 'POSSUM PRODUCTIONS, INC.',
 'POSSUM-BILITY EXPRESS, INCORPORATED',
]
>>> df['Entity Name'] = names[1:]
>>> df['Entity Name'] = names[1:]
>>> df
  Entity Number Registration Date         Status                          Entity Name Jurisdiction      Agent for Service of Process
0      C2645412        04/02/2004         ACTIVE                    POSSUM FILMS, INC      GEORGIA   ERESIDENTAGENT, INC. (C2702827)
1      C0786330        09/22/1976      DISSOLVED                          POSSUM INC.   CALIFORNIA                        I. HALPERN
2      C2334141        03/01/2001  FTB SUSPENDED                   POSSUM MEDIA, INC.   CALIFORNIA                   CLAIR G BURRILL
3      C0658630        11/08/1972  FTB SUSPENDED       POSSUM POINT PRODUCTIONS, INC.   CALIFORNIA                               NaN
4      C1713121        09/23/1992  FTB SUSPENDED             POSSUM PRODUCTIONS, INC.   CALIFORNIA                LAWRENCE J. TURNER
5      C1207820        08/05/1983      DISSOLVED  POSSUM-BILITY EXPRESS, INCORPORATED   CALIFORNIA                          R L CARL
6      C3921531        06/27/2016         ACTIVE                      POSSUMS WELCOME   CALIFORNIA  REGISTERED AGENTS INC (C33658
```

Doing it this way doesn't process the header correctly, so don't forget to ignore the first row, if you need to.
