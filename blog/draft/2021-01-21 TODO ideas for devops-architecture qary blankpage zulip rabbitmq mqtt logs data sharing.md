# TODO ideas for devops/architecture qary blankpage

## Pubsub router on digital ocean

- rabbitmq? https://www.rabbitmq.com/tutorials/tutorial-three-python.html
- mosquitto - EPL/EDL licensed - Eclipse Mosquitto is an open source () message broker that implements the MQTT protocol versions 5.0, 3.1.1 and 3.1
- ActiveMQ - Apache license - popular
- 0MQ - asynchronous messaging library aimed at use in scalable distributed or concurrent applications. It provides a message queue without dedicated broker

## "RealTime Django App for Chatbot using RabbitMQ"

- [Part 1](https://danidee10.github.io/2018/01/01/realtime-django-1.html)
